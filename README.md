# Disclaimer

This was a little project to play around with the tool released by AWS. If you want to run Symfony in an serverless environemtn, have a look at [bref.sh](https://bref.sh). You ca find prebuild runtimes in there and it is quite easy to get started with this.

# About

This repository will contain everything that you need to run a symfony application in Lambda. The layers to run php will be created with a Docker container that can be build with the provided Dockerfile. You don't have to modify that one, but you will have to provide your code as a zip file.

Right now this is just an early draft and it will probably be hard to make a use of this. But I'll try to improve things and document every step as good as possible.

Running Symfony in Lambda was surprisingly easy, but it comes with some limitations.



# How to use

There are a few things to mention:
1. You have to bring your code. The defaulkt location is ./symfony.zip
2. Copy the .env.dist and create your own .env. Change the variables to your likings
3. You don't have to create lambda functions with the make file. Using terraform or something similar is recommended!

So let's start. We'll assume that you changed the .env variables and your application is available in the defined location as a zip archive.

Other prerequisites:
- Docker
- Configured AWS CLI (Make sure you are using the right profile! Also run some command in advance if you are using MFA. The code won't be fetched within the `make` routine)

If it is the first time, run `make init`. This will create the Docker image, buidl the layers and create a lambda function with your code. From now on you will have to use `make deploy` or `make`.

Done.

# How to actually call the lambda

This depends on the use case. If you are using the console handler to fire a command, pass a json like this in the payload of the request:

```
{
  "input": "debug:config doctrine"
}
```

Lambda will now execute the command `debug:config doctrine` and return the result as a JSON response.

But this is different for normal Symfony routes. But it get's awesome in combination with a proxy in the API Gateway. The index.php is already prepared for this and will handle the request completly normal.
If you don't want to use an API Gateway, you'll have to fix this in the `index.php`. But in the end, you just have to fetch the path that you want to call and add it to `Request::create`


## A little collection of commands

**This is all bundled and automated with the make file. But have a look at this stuff if you are interested in how this works**

```
# Build your container
docker build -t lambda-symfony .

# Test your code (will only work if the whole application is bundled)
docker run lambda-symfony public/symfony_start

# Create layers (requires img2lambda)
img2lambda -i lambda-symfony:latest -r eu-central-1 -n symfony

# Zip your code
zip -r symfony.zip src

# Create the function
aws lambda create-function \
    --function-name php-example-symfony \
    --handler public/symfony_start \
    --zip-file fileb://./symfony.zip \
    --runtime provided \
    --role "arn:aws:iam::558902332431:role/service-role/LambdaPhpExample" \
    --region eu-central-1 \
    --layers file://output/layers.json

# Invoke the function
 aws lambda invoke \
    --function-name php-example-symfony \
    --region eu-central-1 \
    --log-type Tail \
    --query 'LogResult' \
    --output text \
    --payload '{"uri":"/"}' hello-output.txt | base64 --decode && cat hello-output.txt

```

# Resources

This repository is based on the example from this repo from AWS: [AWS Lambda Container Image Converter (img2lambda)](https://github.com/awslabs/aws-lambda-container-image-converter)

You can also find an alternative that is using SAM to deploy stuff: [BREF](https://bref.sh/)