#!make
include .env
# export $(shell sed 's/=.*//' envfile)

.DEFAULT_GOAL := deploy_layer
.PHONY := dev deploy_layer init deploy build_docker build_layer run create_lambda update_lambda

# MIXINS
dev: build_docker run
deploy_layer: build_docker build_layer
init: deploy_layer create_lambda
deploy: deploy_layer update_lambda

# Commands
build_docker:
	docker build -t $(DOCKER_TAG) .
build_layer:
	img2lambda -i $(DOCKER_TAG):latest -r $(REGION) -n $(LAYER_NAME)
run: 
	docker run $(DOCKER_TAG) $(HANDLER)


# Just to test things
create_lambda:
	aws lambda create-function \
    --function-name $(FUNCTION_NAME) \
    --handler $(HANDLER) \
    --zip-file fileb://$(FILE) \
    --runtime provided \
    --role "$(ROLE_ARN)" \
    --region $(REGION) \
    --layers file://output/layers.json
update_lambda:
	aws lambda update-function-configuration \
	--function-name $(FUNCTION_NAME) \
	--runtime provided \
    --role "$(ROLE_ARN)" \
    --region $(REGION) \
	--handler $(HANDLER) \
	--layers file://output/layers.json
	aws lambda update-function-code \
	--function-name $(FUNCTION_NAME) \
	--zip-file fileb://$(FILE)