#!/usr/bin/env php
<?php

use App\Kernel as BaseKernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\BufferedOutput;

set_time_limit(0);
ini_set('memory_limit', '3G');

require dirname(__DIR__).'/vendor/autoload.php';

class Kernel extends BaseKernel {
    public function getCacheDir()
    {
        return '/tmp/'.$this->environment.'/cache';
    }

    public function getLogDir()
    {
        return '/tmp/'.$this->environment.'/log';
    }
}

function console($data) {
    try {
        if (!class_exists(Application::class)) {
            throw new RuntimeException('You need to add "symfony/framework-bundle" as a Composer dependency.');
        }
    
        $input = new StringInput($data['input']);
        if ($input === null) {
            $input = "";
        }
        if (null !== $env = $input->getParameterOption(['--env', '-e'], null, true)) {
            putenv('APP_ENV='.$_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = $env);
        }
    
        if ($input->hasParameterOption('--no-debug', true)) {
            putenv('APP_DEBUG='.$_SERVER['APP_DEBUG'] = $_ENV['APP_DEBUG'] = '0');
        }
    
        require dirname(__DIR__).'/config/bootstrap.php';
    
        if ($_SERVER['APP_DEBUG']) {
            umask(0000);
    
            if (class_exists(Debug::class)) {
                Debug::enable();
            }
        }

        $output = new BufferedOutput();
        $kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $application->run($input, $output);
        
        
        return json_encode(
            [
                'statusCode' => 200,
                'body' => preg_replace('!\\r?\\n!', "     ", $output->fetch())
            ]
        );
    } catch (Exception $e) {
        echo $e;
        return $e;
    }
}
