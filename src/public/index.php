<?php

use App\Kernel as BaseKernel;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

// Set the maximum limit to the maximum of Lambda. It's a good idea
// to manage the limits with lambda instead of doing this in the PHP
// interpreter.
ini_set('memory_limit', '3G');
// Max exec time?

require dirname(__DIR__).'/config/bootstrap.php';

class Kernel extends BaseKernel {
    public function getCacheDir()
    {
        return '/tmp/'.$this->environment.'/cache';
    }

    public function getLogDir()
    {
        return '/tmp/'.$this->environment.'/log';
    }
}

function symfony_start($data)
{
    try {
        // Remove the resource from the path. This can be unnecessary if you are using a custom domain name.
        $path = strip_proxy($data['path'], $data['resource']);

        // Make sure a HTTP method is defined
        if ($data['httpMethod'] == '') {
            $data['httpMethod'] = 'GET';
        }

        /**
         * Default Symfony stuff
         */
        if ($_SERVER['APP_DEBUG']) {
            umask(0000);

            Debug::enable();
        }


        if ($trustedProxies = $_SERVER['TRUSTED_PROXIES'] ?? $_ENV['TRUSTED_PROXIES'] ?? false) {
            Request::setTrustedProxies(explode(',', $trustedProxies), Request::HEADER_X_FORWARDED_ALL ^ Request::HEADER_X_FORWARDED_HOST);
        }

        if ($trustedHosts = $_SERVER['TRUSTED_HOSTS'] ?? $_ENV['TRUSTED_HOSTS'] ?? false) {
            Request::setTrustedHosts([$trustedHosts]);
        }

        $kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
    
        /**
         * End of default Symfony stuff
         */
    
        // Create the request manually, the globals are not properly passed
        $request = Request::create($path, $data['httpMethod']);
        $response = $kernel->handle($request);
    
        // Don't send the response, but return it in the way the API Gateway expects it
        return json_encode([
            'isBase64Encoded' => false,
            'statusCode' => $response->getStatusCode(),
            'multiValueHeaders' => $response->headers->all(),
            'body' => $response->getContent()
        ]);
    } catch (Exception $e) {
        echo print_r($e, true);
        return print_r($e, true);
    }
}

/**
 * Remove the resource from the path if it is present
 */
function strip_proxy($path, $resource) {
    // Turn /{resource+} into /resource
    $resource = preg_replace('/([\{\}\+])+/', "", $resource);

    // Replace the resource with an empty string if it is present
    return str_replace($resource, '', $path);
}